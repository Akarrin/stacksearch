package com.example.lukas.stacksearch;

import java.io.Serializable;

/**
 * Created by lukas on 15.05.2017.
 */

public class Question implements Serializable {

    private String questionLink;
    private String avatarLink;
    private String authorName;
    private String title;
    private int answerCount;

    public Question(String questionLink, String avatarLink, String authorName, String title, int answerCount) {
        this.questionLink = questionLink;
        this.avatarLink = avatarLink;
        this.authorName = authorName;
        this.title = title;
        this.answerCount = answerCount;
    }

    public String getQuestionLink() {
        return questionLink;
    }

    public String getAvatarLink() {
        return avatarLink;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getTitle() {
        return title;
    }

    public int getAnswerCount() {
        return answerCount;
    }
}
