package com.example.lukas.stacksearch;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ViewAnimator;

import com.example.lukas.stacksearch.stackoverflow.SearchResponse;
import com.example.lukas.stacksearch.stackoverflow.StackOverflowService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements Callback<SearchResponse> {

    @BindView(R.id.view_switcher) ViewAnimator viewAnimator;

    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    @BindView(R.id.search_edittext) EditText searchEditText;

    @BindInt(R.integer.max_answer_count) int maxAnswerCount;

    private QuestionsListAdapter questionsListAdapter;
    private List<Question> questions;

    private Retrofit retrofit;
    private StackOverflowService stackOverflowService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restoreFromBundle(savedInstanceState);
        initViews();
        initRetrofit();
    }

    private void restoreFromBundle(Bundle bundle) {
        if (bundle!=null) {
            questions = (List<Question>) bundle.getSerializable("questions");
        } else {
            questions = new ArrayList<>();
        }
    }

    private void initViews() {
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initRecyclerView();
        initViewAnimator();
    }

    private void initRetrofit() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl("http://api.stackexchange.com/2.2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.stackOverflowService = retrofit.create(StackOverflowService.class);
    }

    private void initRecyclerView() {
        questionsListAdapter = new QuestionsListAdapter(this, questions);
        recyclerView.setAdapter(questionsListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initViewAnimator() {
        if (questions.isEmpty()) {
            showNoContent();
        } else {
            showRecyclerView();
        }
    }

    private void showNoContent() {
        viewAnimator.setDisplayedChild(0);
    }

    private void showProgressBar() {
        viewAnimator.setDisplayedChild(1);
    }

    private void showRecyclerView() {
        viewAnimator.setDisplayedChild(2);
    }

    @OnClick(R.id.search_button)
    public void search() {
        String intitle = searchEditText.getText().toString();
        stackOverflowService.searchIntitle(intitle).enqueue(this);
        showProgressBar();
        Keyboard.hide(this);
    }

    @Override
    public void onResponse(@NonNull Call<SearchResponse> call, @NonNull Response<SearchResponse> response) {
        questions.clear();
        if (response.body()!=null) {
            for (SearchResponse.Item item : response.body().items) {
                questions.add(new Question(item.link, item.owner.profile_image, item.owner.display_name, item.title, item.answer_count));
                if (questions.size()>=maxAnswerCount) {
                    break;
                }
            }
        }
        if (questions.isEmpty()) {
            showNoContent();
        } else {
            questionsListAdapter.notifyDataSetChanged();
            showRecyclerView();
        }
}

    @Override
    public void onFailure(Call<SearchResponse> call, Throwable t) {
        showNoContent();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("questions", (Serializable) questions);
    }
}
