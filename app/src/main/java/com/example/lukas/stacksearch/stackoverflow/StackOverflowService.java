package com.example.lukas.stacksearch.stackoverflow;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by lukas on 15.05.2017.
 */

public interface StackOverflowService {

    @GET("/search?order=desc&sort=activity&site=stackoverflow")
    Call<SearchResponse> searchIntitle(@Query("intitle") String intitle);
}
