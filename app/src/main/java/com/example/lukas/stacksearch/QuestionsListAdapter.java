package com.example.lukas.stacksearch;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lukas on 15.05.2017.
 */

public class QuestionsListAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<Question> questions;

    public QuestionsListAdapter(Context context, List<Question> questions) {
        this.context = context;
        this.questions = questions;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder2, int position) {
        ViewHolder holder = (ViewHolder)holder2;
        Question question = questions.get(position);
        holder.setOnClickLink(question.getQuestionLink());
        holder.loadImage(question.getAvatarLink());
        holder.setAuthorName(question.getAuthorName());
        holder.setTitle(question.getTitle());
        holder.setAnswersCount(question.getAnswerCount());
        holder.setDividerVisible(position<questions.size()-1);
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar_image) ImageView avatarImage;

        @BindView(R.id.author_name) TextView authorName;

        @BindView(R.id.title) TextView title;

        @BindView(R.id.answers_count) TextView answersCount;

        @BindView(R.id.divider) View divider;

        private View itemView;

        private ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }

        private void setOnClickLink(final String link) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, WebViewActivity.class);
                    intent.putExtra("link", link);
                    context.startActivity(intent);
                }
            });
        }

        private void loadImage(String link) {
            Picasso.with(context)
                    .load(link)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(avatarImage);
        }

        private void setAuthorName(String authorName) {
            this.authorName.setText(authorName);
        }

        private void setTitle(String title) {
            this.title.setText(title);
        }

        private void setAnswersCount(int answersCount) {
            this.answersCount.setText(String.valueOf(answersCount));
        }

        private void setDividerVisible(boolean visible) {
            this.divider.setVisibility(visible?View.VISIBLE:View.INVISIBLE);
        }
    }
}
