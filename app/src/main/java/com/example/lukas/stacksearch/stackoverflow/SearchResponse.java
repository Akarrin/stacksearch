package com.example.lukas.stacksearch.stackoverflow;

import java.util.List;

/**
 * Created by lukas on 16.05.2017.
 */

public class SearchResponse {

    public List<Item> items;

    public class Item {
        public Owner owner;
        public int answer_count;
        public String link;
        public String title;

        public class Owner {
            public String profile_image;
            public String display_name;
        }
    }
}
